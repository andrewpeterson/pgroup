#!/usr/bin/env python
"""Simplified POV-RAY environment creator."""

import os
import numpy as np
from ase.data.colors import jmol_colors
from ase.data import covalent_radii


class POV:
    """Class to creat .pov files to be executed with the pov-ray raytracer.
    Initialize with

    atoms : an ase atoms object

    Keyword arguments: (all distances in Angstroms)
    ------------------
    tex : a texture to use for the atoms, either a single value or a list
        of len(atoms), default = 'vmd'
    radii : atomic radii. if a single value is given, it is interpreted as
        a multiplier for the covalent radii in ase.data. if a list of
        len(atoms) is given, it is interpreted as individual atomic radii.
        default = 1.0
    colors : a list of len(atoms) of the colors, as (r,g,b). default is
        None which will use ASE standard colors
    cameratype : type of povray camera, default='perspective'
    cameralocation : location of camera as an (x,y,z) tuple.
        This can control the zoom.
        default = (0., 0., 20)
    look_at : where the camera is pointed at as an (x,y,z) tuple.
        default = (0., 0., 0.)
    camera_right_up : the right and up vectors that define the image
        boundaries. The right:up ratio will also define the aspect ratio
        (width:height) of the resulting image. These two vectors should
        generally be orthogonal -- generaly on the x,y plane.
        default = [(-8.,0.,0.),(0.,6.,0.)]
    cameradirection : the initial direction vector of the camera before
        it is moved with look_at. This will also control the zoom, with
        higher values being more zoomed in. default = (0., 0., 10.)
    area_light : location and parameters of area light as [(x,y,x), color,
        width, height, Nlamps_x, Nlamps_y], default = [(20., 3., 40.),
        'White', .7, .7, 3, 3]
    background : background color, default = 'White'
    bondatoms : list of atoms to be bound together, as in
        [(index1, index2), ...], default = None
    bondradii : radii to use in drawing bonds, default = 0.1
    pixelwidth : width in pixels of the final image. Note that the height
        is set by the aspect ratio (controlled by carmera_right_up).
        This controls the resolution.
        default = 320
    clipplane : plane at which to clip atoms, for example "y, 0.00".
        default = None
    """

    _default_settings = {
        'tex': 'vmd',
        'radii': 1.,
        'colors': None,
        'cameratype': 'perspective',
        'cameralocation': (0., 0., 20.),
        'look_at': (0., 0., 0.),
        'camera_right_up': [(-8., 0., 0.), (0., 6., 0.)],
        'cameradirection': (0., 0., 10.),
        'area_light': [(20., 3., 40.), 'White', .7, .7, 3, 3],
        'background': 'White',
        'bondatoms': None,
        'bondradius': .1,
        'pixelwidth': 320,
        'clipplane': None,
    }

    def __init__(self, atoms, **kwargs):
        for k, v in self._default_settings.items():
            setattr(self, '_' + k, kwargs.pop(k, v))
        if len(kwargs) > 0:
            print(kwargs)
            raise TypeError('POV got one or more unexpected keywords.')
        self._atoms = atoms
        self._numbers = atoms.get_atomic_numbers()
        if self._colors is None:
            self._colors = jmol_colors[self._numbers]
        if (type(self._radii) is float) or (type(self._radii) is int):
            self._radii = covalent_radii[self._numbers] * self._radii
        if self._bondatoms is None:
            self._bondatoms = []
        self._aspectratio = (np.linalg.norm(self._camera_right_up[0]) /
                             np.linalg.norm(self._camera_right_up[1]))

    def write(self, filename, run_povray=None):
        """Writes out the .pov file for ray-tracing and also an associated
        .ini file. If filename ends in ".png" it will run povray to turn it
        into a png file. If the filename ends in ".pov" it will not. This can
        be overridden with the keyword run_povray.
        """
        if filename.endswith('.png'):
            filename = filename[:-4] + '.pov'
            if run_povray is None:
                run_povray = True
        elif filename.endswith('.pov'):
            if run_povray is None:
                run_povray = False
        else:
            raise RuntimeError('filename must end in .pov or .png')
        self._filename = filename
        filebase = filename[:-4]
        # Write the .pov file.
        f = open(filebase + '.pov', 'w')

        def w(text):
            f.write(text + '\n')

        w('#include "colors.inc"')
        w('#include "finish.inc"')
        w('')
        w('global_settings {assumed_gamma 1 max_trace_level 6}')
        w('background {color %s}' % self._background)
        w('camera {%s' % 'perspective')
        w('  location <%.2f,%.2f,%.2f>' % tuple(self._cameralocation))
        camera_right_up = self._camera_right_up
        w('  right <%.2f,%.2f,%.2f> up <%.2f,%.2f,%.2f>' %
          (camera_right_up[0][0], camera_right_up[0][1],
           camera_right_up[0][2], camera_right_up[1][0],
           camera_right_up[1][1], camera_right_up[1][2]))
        w('  direction <%.2f,%.2f,%.2f>' % tuple(self._cameradirection))
        w('  look_at <%.2f,%.2f,%.2f>}' % tuple(self._look_at))
        w('light_source {<%.2f,%.2f,%.2f> color %s' %
          tuple(list(self._area_light[0]) + [self._area_light[1]]))
        w('  area_light <%.2f,0,0>, <0,%.2f,0>, %i, %i' %
          tuple(self._area_light[2:]))
        w('  adaptive 1 jitter}')
        w('')
        w('#declare simple = finish {phong 0.7}')
        w('#declare pale = finish {ambient .5 diffuse .85 roughness .001 specular 0.200 }')
        w('#declare intermediate = finish {ambient 0.3 diffuse 0.6 specular 0.60 roughness 0.04 }')
        w('#declare vmd = finish {ambient .0 diffuse .65 phong 0.1 phong_size 40. specular 0.500 }')
        w('#declare jmol = finish {ambient .2 diffuse .6 specular 1 roughness .001 metallic}')
        w('#declare ase2 = finish {ambient 0.05 brilliance 3 diffuse 0.6 metallic specular 0.70 roughness 0.04 reflection 0.15}')
        w('#declare ase3 = finish {ambient .15 brilliance 2 diffuse .6 metallic specular 1. roughness .001 reflection .0}')
        w('#declare glass = finish {ambient .05 diffuse .3 specular 1. roughness .001}')
        w('#declare Rbond = %.3f;' % self._bondradius)
        w('')
        if self._clipplane is None:
            w('#macro atom(LOC, R, COL, FIN)')
            w('  sphere{LOC, R texture{pigment{COL} finish{FIN}}}')
            w('#end')
        else:
            w('#macro atom(LOC, R, COL, FIN)')
            w('  difference{')
            w('   sphere{LOC, R}')
            w('   plane{%s}' % self._clipplane)
            w('   texture{pigment{COL} finish{FIN}}')
            w('  }')
            w('#end')
        w('')

        for atom in self._atoms:
            w('atom(<%.2f,%.2f,%.2f>, %.2f, rgb <%.2f,%.2f,%.2f>, %s) // #%i'
              % (atom.x, atom.y, atom.z,
                 self._radii[atom.index], self._colors[atom.index][0],
                 self._colors[atom.index][1], self._colors[atom.index][2],
                 self._tex, atom.index))

        for bond in self._bondatoms:
            pos0 = self._atoms[bond[0]].position.copy()
            pos1 = self._atoms[bond[1]].position.copy()
            middle = (pos0 + pos1) / 2.
            color0 = self._colors[bond[0]]
            color1 = self._colors[bond[1]]
            w('cylinder {<%.2f,%.2f,%.2f>, <%.2f,%.2f,%.2f>, Rbond '
              'texture{pigment {rgb <%.2f,%.2f,%.2f>} finish{%s}}} '
              ' // # %i to %i' %
              (pos0[0], pos0[1], pos0[2],
               middle[0], middle[1], middle[2],
               color0[0], color0[1], color0[2], self._tex,
               bond[0], bond[1]))
            w('cylinder {<%.2f,%.2f,%.2f>, <%.2f,%.2f,%.2f>, Rbond '
              'texture{pigment {rgb <%.2f,%.2f,%.2f>} finish{%s}}} '
              ' // # %i to %i' %
              (middle[0], middle[1], middle[2],
               pos1[0], pos1[1], pos1[2],
               color1[0], color1[1], color1[2], self._tex,
               bond[0], bond[1]))
        f.close()

        # Write the .ini file.
        f = open(filebase + '.ini', 'w')
        w('Input_File_Name=%s' % os.path.split(filename)[1])
        w('Output_to_File=True')
        w('Output_File_Type=N')
        w('Output_Alpha=False')
        w('Width=%d' % self._pixelwidth)
        w('Height=%.0f' % (self._pixelwidth / self._aspectratio))
        w('Antialias=True')
        w('Antialias_Threshold=0.1')
        w('Display=False')
        w('Pause_When_Done=True')
        w('Verbose=False')

        f.close()
        if run_povray:
            self.raytrace(filename)

    def raytrace(self, filename=None):
        """Run povray on the generated file."""

        if not filename:
            filename = self._filename
        filebase = filename[:-4]
        if os.path.split(filename)[0] != '':
            pwd = os.getcwd()
            os.chdir(os.path.split(filename)[0])
        os.system('povray %s.ini' % os.path.split(filebase)[1])
        if os.path.split(filename)[0] != '':
            os.chdir(pwd)
