import os
from ase import io
from ase.neb import NEB
from ase.optimize import BFGS, QuasiNewton
from ase.parallel import barrier


class ReNEB:
    """Class for ASE NEB calculations to allow no-hassle resubmission when
    the job times out. This class should automatically find where the
    previous script left off and resume. Initialize with:

        images: list of ASE atoms objects; this is the 'band' and they
            should already be interpolated into the initial guess

        calcfxn: a function that, when fed the atoms, will attach a
            calculator. Should also take as argument an integer which
            will be used to label the individual calculators.

        constraintfxn: a function that, when fed the atoms, will apply the
            desired constraints to the atoms.

        label: (string) a label to use when saving NEB files
    """

    def __init__(self, images, calcfxn, constraintfxn, label='neb'):
        self._images = images
        self._label = label
        # Check if requeuing.
        started = False
        files = os.listdir(os.getcwd())
        trajnames = [file for file in files if
                     (file.startswith(label) and file.endswith('.traj'))]
        trajnames.sort()
        if len(trajnames) > 0:
            started = True
        # Find last images if requeuing.
        if started:
            trajname = trajnames[-1]
            self._count = int(trajname.split('-')[-1].split('.')[0]) + 1
            images = [io.read(trajname, index=index) for index in
                      range(-len(images), 0)]
            self._neb = NEB(images)
            if 'climb' in trajname:
                self._neb.climb = True
        else:
            self._count = 0
            self._neb = NEB(images)
        # Set calculator and constraints on internal images.
        for index in range(1, len(images) - 1):
            calcfxn(images[index], index)
            constraintfxn(images[index])

    def run(self, fmax=0.05, fmax_climb=None, Optimizer=None):
        """Runs the NEB. If both fmax's are specified, it first does
        standard (non-climbing) and then climbing after fmax is reached. If
        only one of the fmax's is specified, only does that one. You can
        also specify which ASE optimizer to use by passing it as a class;
        default is BFGS."""
        if not Optimizer:
            Optimizer = BFGS
        if fmax:
            filebase = '%s-%02i' % (self._label, self._count)
            opt = Optimizer(self._neb, trajectory=filebase + '.traj',
                            logfile=filebase + '.log')
            opt.run(fmax=fmax)
            self._count += 1
        if fmax_climb:
            self._neb.climb = True
            filebase = '%s-%02i-climb' % (self._label, self._count)
            opt = Optimizer(self._neb, trajectory=filebase + '.traj',
                            logfile=filebase + '.log')
            opt.run(fmax=fmax_climb)


class ReOpt:
    """Class for ASE optimizer calculations to allow no-hassle resubmission when
    the job times out. This class should automatically find where the
    previous script left off and resume. Initialize with:

        atoms: the ASE atoms object. This should have a calculator attached
            as well as all constraints.

        label: (string) a label to use when saving optimizer files
    """

    def __init__(self, atoms, label='opt'):
        self._atoms = atoms
        self._label = label
        # Check if requeuing.
        started = False
        files = os.listdir(os.getcwd())
        trajnames = [file for file in files if
                     (file.startswith(label) and file.endswith('.traj'))]
        trajnames.sort()
        print(trajnames)
        if len(trajnames) > 0:
            started = True
        # FIXME: I had an erratic error here once. I suspect what happened
        # is one processor got ahead of the rest, created the trajectory,
        # and a slower processor then saw this empty trajectory and thought
        # it was a re-start (it was actually a fresh run). This was with
        # GPAW; need to consider how to fix.
        # Find last image if requeuing.
        if started:
            trajname = trajnames[-1]
            self._count = int(trajname.split('-')[-1].split('.')[0]) + 1
            atoms = io.read(trajname, index=-1)
            assert (atoms.get_chemical_symbols() ==
                    self._atoms.get_chemical_symbols())
            self._atoms.set_positions(atoms.get_positions())
        else:
            self._count = 0
        barrier()

    def run(self, fmax=0.05, Optimizer=QuasiNewton):
        """Runs the optimizer, with specified fmax. A custom optimizer can
        be specified, otherwise defaults to ASE's QuasiNewton.
        """
        filebase = '%s-%02i' % (self._label, self._count)
        opt = Optimizer(self._atoms, trajectory=filebase + '.traj',
                        logfile=filebase + '.log')
        opt.run(fmax=fmax)
        self._count += 1
