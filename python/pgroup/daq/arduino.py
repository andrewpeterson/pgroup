"""Modules of use with our arduino system."""

import serial
import sys
import signal
import warnings
import time
import numpy as np
from xml.dom.minidom import parseString
from xml.parsers.expat import ExpatError
from datetime import datetime, timedelta
from matplotlib import pyplot
from matplotlib.ticker import MaxNLocator
from matplotlib.dates import DateFormatter


class TemperatureLogger:
    """Routine to interface with the arduino and collect temperature
    measurements.

    channels is the list of channels to sample on the arduino, as in
    [2, 3, 4, 5]
    Note that the arduino should be set up to read the temperature, in C,
    when the integer of the channel number is sent to it followed by
    a new line character.

    Other optional inputs:
    - delay (float): time in seconds to wait between samples
    - serial (string): path to serial connection with the arduino
    - logfile (string): path to the text file which will be written in
      append mode
    - print_to_screen (True/False): also print output to screen
    - plot (True/False, or plot object): if true, generates a generic plot
      with the name 'out.pdf' in the current directory. If false, makes no
      plot. Can also be fed any plotting object with a method:
          object.plot(time, temperatures)
      where time is a datetime object and temperatures is a dictionary of
      temperatures, all at the current data point.
    """

    def __init__(self, channels, delay=10.0, serial='/dev/ttyACM3',
                 logfile='out.txt', print_to_screen=True, plot=True):
        self._channels = channels 
        self._delay = delay
        self._serial = serial
        self._logfile = logfile
        self._print_to_screen = print_to_screen
        self._stored_data = {}
        if plot == True:
            self._plot = SavePlot()
        elif plot:
            self._plot = plot
        else:
            self._plot = None

    def run(self, maxreadings=None):
        """When called, the routine will run until maxreadings is reached.
        It is fine to quit early with CTRL-C; this will safely close the
        serial connection."""
        signal.signal(signal.SIGINT, self._signal_handler)
        self._arduino = serial.Serial(self._serial, 9600, timeout=2)
        loop = True
        readings = 0
        self._log()
        while loop:
            temperatures = self._read_temperatures()
            now = datetime.now()
            self._log(now, temperatures)
            if self._plot:
                self._plot.plot(now, temperatures)
            time.sleep(self._delay)
            if maxreadings:
                readings += 1
                if readings > maxreadings:
                    loop = False
        self._shutdown()

    def _log(self, time=None, temperatures=None):
        """Performs logging actions as requested by the user. If time and
        temperatures are None, prints header line only."""
        if (time == None) and (temperatures == None):
            f = open(self._logfile, 'a')
            f.write('*' * 80 + '\n')
            f.write('Log data in tab-separated format.\n')
            f.write('Values are in Celsius.\n' + '*'*80 + '\n')
            screenheader = '%9s'
            fileheader = '%s'
            for channel in self._channels:
                screenheader += '%8i'
                fileheader += '\t%i'
            fileheader += '\n'
            print(screenheader % tuple(['time'] + self._channels))
            f.write(fileheader % tuple(['time'] + self._channels))
            f.close()
            return
        screenline = '%9s'
        fileline = '%s'
        temperaturelist = []
        for channel in self._channels:
            screenline += '%8.2f'
            fileline += '\t%.2f'
            temperaturelist += [temperatures[channel]]
        fileline += '\n'
        print(screenline % tuple([time.strftime('%H:%M:%S')] + temperaturelist))
        f = open(self._logfile, 'a')
        f.write(fileline % tuple([time.isoformat()] + temperaturelist))
        f.close()

    def _signal_handler(self, signal, frame):
        """Routine to close the serial connection if process is interrupted."""
        print('*' * 70)
        print('Keyboard interrupt received. Performing shutdown.')
        self._shutdown()
        print('Exiting.')
        sys.exit(0)

    def _shutdown(self):
        """Safely closes the serial connection. Called automatically when
        sampling terminates or CTRL-C is sent."""
        print('Closing serial connection.')
        self._arduino.close()
        print('Serial connection closed.')

    def _read_temperatures(self):
        """Pulls a line from the arduino, makes sure it matches the formatting,
        then converts it to the temperature data. If it doesn't match, pulls
        the next line, etc."""
        temperatures = {}
        for channel in self._channels:
            temperatures[channel] = self._read_temperature(channel)
        return temperatures

    def _read_temperature(self, channel, maxtries=10):
        """Reads a single temperature from the arduino. Retries if bad line
        encountered."""
        tries = 0
        while tries < maxtries:
            self._arduino.write('%i\n' % channel)
            line = self._arduino.readline()
            try:
                temperature = float(line)
                return temperature
            except ValueError:
                tries += 1
        return np.nan


class SavePlot:
    """A plot that is dynamically updated and saved to disk. In combination
    with evince, should provide real-time plotting capabilities. Keywords:

    variables (list of strings) - variables to be plot on the y axis. The
      variables will be the keys to the dictionary fed to self.plot. If
      None, plots everything.
    points (integer) - number of most recent points to plot
    """

    def __init__(self, filepath='out.pdf', variables=None, points=1200):
        self.filepath = filepath
        self._variables = variables
        self._points = points
        # Data storage.
        self._times = []
        if self._variables:
            self._temperatures = {}
            for variable in self._variables:
                self._temperatures[variable] = []

    def plot(self, time, temperatures):
        """Adds the time and list of temperatures to the plot and refreshes
        it. The time should be a datetime.datetime object, and the
        temperatures should be a dictionary of floats. The keys of the
        temperatures dictionary that are stored in self.variables will be
        plotted."""
        if not self._variables:
            self.set_variables(temperatures)
        self._times.append(time)
        if len(self._times) > self._points:
            self._times.pop(0)
        for variable in self._variables:
            self._temperatures[variable].append(temperatures[variable])
            if len(self._temperatures[variable]) > self._points:
                self._temperatures[variable].pop(0)
        fig = pyplot.figure()
        ax = fig.add_subplot(111)
        for variable in self._variables:
            ax.plot_date(self._times, self._temperatures[variable], '-',
                    label=str(variable))
        ax.set_ylabel('$T$, $^\circ$C')
        ax.legend(loc='best')
        ax.xaxis.set_major_locator(MaxNLocator(4))
        ax.xaxis.set_major_formatter(DateFormatter('%H:%M'))
        fig.savefig(self.filepath)
        pyplot.close() # required to prevent memory leak

    def set_variables(self, temperatures):
        """Sets the plot to use all the keys of temperatures as
        variables."""
        keys = temperatures.keys()
        keys.sort()
        self._variables = keys
        self._temperatures = {}
        for variable in self._variables:
            self._temperatures[variable] = []
