"""Scripts to summarize the ASE output."""

import os
import shutil
from string import Template
from random import randint
from ase import io

from .nicesort import sort_nicely
from .latextemplates import template1, template2


def summarize_ase_output(path='.', referenceenergy=None,
                         outputfile='Summary.pdf',
                         deletetempdirectory=True, paths=None,
                         sort='alphanumerical',
                         fmax=0.05):
    """Produces a pdf book of the ASE output in all subdirectories of the
    specified path. If referenceenergy is supplied, all energies subtract
    off that reference.  referenceenergy can also be 'min' or 'max' to
    subtract off the minimum or maximum energy in the list. If
    deletetempdirectory is False, then all the images used to make the
    summary book will be left.  Optionally, a list of paths can be provided
    from which to make the summaries. It autogenerates from the first-level
    subdirectories if not supplied. Sort can be either 'alphabetical' or
    'energy', which is the order the pages appear in the book.
    If fmax is supplied, looks in the last alphabetical *.traj file, and if
    fmax is less than this value assumes the run is converged. If fmax is
    None, uses instead the finishedflag.
    """
    finishedflag = '.energy'  # If present in a subdir, the program acts
    imagefileflag = '.traj'  # If more than one imagefile is found, it
                             # assumes they are in alphabetical order and
                             # takes the initial image from the first
                             # and the final image from the last.

    # Make a temporary directory for all the scratch output.
    tmpdir = 'tmp' + str(randint(1000000000, 9999999999))
    os.mkdir(tmpdir)
    os.mkdir(os.path.join(tmpdir, 'pages'))

    # Find subdirectories that are not the temp directory, unless paths
    # was supplied.
    if not paths:
        subdirs = [name for name in os.listdir(path) if
                   os.path.isdir(os.path.join(path, name))]
        subdirs = [name for name in subdirs if name != tmpdir]
        sort_nicely(subdirs)
    else:
        subdirs = paths
        for index, path in enumerate(subdirs):
            if path[0] == '/':
                subdirs[index] = subdirs[index][1:]

    energy = {}
    imagesproduced = {}
    converged = {}
    for subdir in subdirs:
        print(subdir)
        # Find if the simulation converged.
        converged[subdir] = False
        imagesproduced[subdir] = False
        imagefiles = []
        for name in os.listdir(os.path.join(path, subdir)):
            if finishedflag in name:
                converged[subdir] = True
                energyfile = os.path.join(path, subdir, name)
            elif imagefileflag in name:
                imagefiles.append(os.path.join(path, subdir, name))
                if os.path.getsize(os.path.join(path, subdir, name)) > 0:
                    imagesproduced[subdir] = True
        # imagefiles = sorted(imagefiles) # alphabetize
        print(' Converged = ' + str(converged[subdir]))
        print(' Images    = ' + str(imagesproduced[subdir]))
        # Process the simulations with data.
        if converged[subdir]:
            # Extract the energy.
            f = open(energyfile, 'r')
            energy[subdir] = eval(f.read())
            f.close()
        else:
            energy[subdir] = None
        if imagesproduced[subdir]:
            # Make figures of the atomic position.
            os.makedirs(os.path.join(tmpdir, subdir))
            makepdfsfromtraj(startfile=imagefiles[0],
                             endfile=imagefiles[-1],
                             outdir=os.path.join(tmpdir, subdir))

    # Correct energies for reference energy
    newenergy = {}
    if sum([type(value) == float for value in energy.values()]) != 0:
        minenergy = min([value for value in energy.values() if
                         type(value) == float])
        maxenergy = max([value for value in energy.values() if
                         type(value) == float])
    if referenceenergy is None:
        referenceenergy = 0
    elif referenceenergy == 'min':
        referenceenergy = minenergy
    elif referenceenergy == 'max':
        referenceenergy = maxenergy
    for key in energy.keys():
        if type(energy[key]) == float:
            newenergy[key] = energy[key] - referenceenergy

    # Report the energies to the user to the standard output and to PDF
    for subdir in subdirs:
        print(subdir + ': ' + str(energy[subdir]))
        cleantext = latex_escape_characters(subdir)
        if converged[subdir]:
            txt = "\\textbf{" + cleantext + ':}' + '\n\n' + \
                  '%.3f' % newenergy[subdir] + ' eV' + '\n\n' + \
                  'Absolute:  %.3f' % energy[subdir] + '\n\n' + \
                  'Reference: %.3f' % referenceenergy + '\n\n' + \
                  'Minimum:   %.3f' % minenergy + \
                  ' (%.3f)' % (minenergy-referenceenergy) + '\n\n' + \
                  'Maximum:   %.3f' % maxenergy + \
                  ' (%.3f)' % (maxenergy-referenceenergy)
        else:
            txt = "\\textbf{" + cleantext + ':}' + '\n\n' + \
                  'Not converged.'
        if imagesproduced[subdir]:
            summarypdfpage(indir=os.path.join(tmpdir, subdir),
                           outpath=os.path.join(tmpdir, 'pages',
                                                "__".join(os.path.split(
                                                    subdir))
                                                + '.pdf'),
                           customtext={'text': txt})

    # Combine all of the pdfs in the pages directory into a book.
    pwd = os.getcwd()
    os.chdir(os.path.join(tmpdir, 'pages'))
    template = Template(template2)

    imagessubdirs = [subdir for subdir in subdirs if
                     imagesproduced[subdir]]
    if sort == 'alphanumerical':
        sort_nicely(imagessubdirs)
    elif sort == 'energy':
        have_energy = [subdir for subdir in imagessubdirs if
                       type(energy[subdir]) == float]
        no_energy = [subdir for subdir in imagessubdirs if
                     type(energy[subdir]) != float]
        sortme = zip([energy[subdir] for subdir in have_energy],
                     have_energy)
        sortme.sort()
        nothing, imagessubdirs = zip(*sortme)
        imagessubdirs = list(imagessubdirs)
        print(imagessubdirs)
        sort_nicely(no_energy)
        imagessubdirs = imagessubdirs + no_energy
        print(imagessubdirs)

    else:
        raise RuntimeError('Bad sort option: %s.' % sort)

    text = ''
    for subdir in imagessubdirs:
        text = text + '\\includepdf{' + \
          "__".join(os.path.split(subdir)) + '.pdf}\n'

    f = open('combine_pdfs.tex', 'w')
    f.write(template.substitute({'text': text}))
    f.close()
    del f

    print('here')
    os.system('pdflatex --interaction batchmode combine_pdfs.tex')
    os.chdir(pwd)

    # Copy the pdf book out into the original directory
    shutil.copy(os.path.join(tmpdir, 'pages', 'combine_pdfs.pdf'),
                outputfile)
    if deletetempdirectory is True:
        shutil.rmtree(tmpdir)


def makepdfsfromtraj(startfile, endfile, outdir='out'):
    """Creates a set of PDFs from a starting (startfile) and an ending
    (endfile) trajectory file, in both the initial
    and final geometry in top, front, side, and perspective angles."""


    def writeperspectives(atoms, basename, outdir):
        """Writes out files for the four main perspectives for Atoms
        object atoms, and names them with basename basename and puts
        them in output directory outdir."""
        join = os.path.join
        write = io.write
        write(filename=join(outdir,basename + '_top.eps'),
                  images=atoms,
                  format='eps',)
        write(filename=join(outdir,basename + '_front.eps'),
                  images=atoms,
                  format='eps',
                  rotation='-90x')
        write(filename=join(outdir,basename + '_side.eps'),
                  images=atoms,
                  format='eps',
                  rotation='-90x,90y,0z')
        write(filename=join(outdir,basename + '_perspective.eps'),
                  images=atoms,
                  format='eps',
                  rotation='-70x,45y,10z')
        files = [basename + '_top.eps',
                 basename + '_front.eps',
                 basename + '_side.eps',
                 basename + '_perspective.eps',]
        pwd = os.getcwd()
        os.chdir(outdir)
        for file in files:
            os.system('epstopdf ' + file)
            os.remove(file)
        os.chdir(pwd)

    # Initial geometry.
    atoms = io.read(startfile, index=0)
    writeperspectives(atoms=atoms, basename='initial', outdir=outdir)
    # Final geometry.
    atoms = io.read(endfile, index=-1)
    writeperspectives(atoms=atoms, basename='final', outdir=outdir)


def summarypdfpage(indir, outpath, customtext):
    """Makes a one-page pdf from the perspective files generated by
    makepdfsfromtraj(). indir is the directory where the input
    pdfs are stored, and outpath is the directory and filename in which
    to put the resulting pdf. customtext['text'] is the text to go
    in the textbox on the pdf."""

    # Import the template into a template object.
    template = Template(template1)

    # Put the tex file into the image directory and run it.
    pwd = os.getcwd()
    os.chdir(indir)
    f = open('compile.tex', 'w')
    f.write(template.substitute(customtext))
    f.close()
    del f
    os.system('pdflatex --interaction batchmode compile.tex')
    os.chdir(pwd)

    # Copy the resulting pdf to the appropriate location.
    shutil.copy(os.path.join(indir, 'compile.pdf'), outpath)


def latex_escape_characters(text):
    r"""Handles trouble characters in latex. Currently only replaces the
    underscore ("_") with an escaped underscore ("\_")."""
    cleantext = ""
    for character in text:
        if character == '_':
            cleantext += r'\_'
        else:
            cleantext += character
    return cleantext
