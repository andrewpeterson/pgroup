

template1 = r"""
\documentclass{article}
\usepackage[landscape]{geometry}
\usepackage{graphicx}
\setlength{\parindent}{0in}
\begin{document}

\begin{minipage}[t]{6.1in}
\noindent 
\includegraphics[width=1.0in]{initial_top.pdf}
\includegraphics[width=1.0in]{initial_front.pdf}
\includegraphics[width=1.0in]{initial_side.pdf}
\includegraphics[width=1.0in]{initial_perspective.pdf}
\\

\noindent 
\includegraphics[width=1.0in]{final_top.pdf}
\includegraphics[width=1.0in]{final_front.pdf}
\includegraphics[width=1.0in]{final_side.pdf}
\includegraphics[width=1.0in]{final_perspective.pdf}
\end{minipage}
\begin{minipage}[t]{2.1in}
${text}
\end{minipage}


\end{document}
"""

template2 = r"""
\documentclass[landscape]{article}
\usepackage{pdfpages}
\begin{document}
${text}
\end{document}
"""

template2p = r"""
\documentclass{article}
\usepackage{pdfpages}
\begin{document}
${text}
\end{document}
"""
