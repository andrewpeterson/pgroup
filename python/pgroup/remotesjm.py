#!/usr/bin/env python3
"""
Contains modules for using SJM (in GPAW) as a remote-controlled calculator.
Use this in a script that you submit with regular (non-parallel)
python; the branching out into parallel mode is done behind the
scenes.

Use in your scripts as below --- essentially just like a regular
calculator.

from pgroup.remotesjm import RemoteSJM

<regular build atoms stuff here>
calc = RemoteSJM(totalcores=24,  # insert correct values
                  <rest of SJM parameters here,
                  )
atoms.set_calculator(calc)

<regular atomistic stuff here>
"""

import sys
import zmq
import time
import threading
import subprocess
from socket import gethostname
from tempfile import NamedTemporaryFile

from gpaw.solvation.sjm import SJM 
from ase.parallel import world, paropen
from ase.calculators.calculator import Calculator, all_changes


class RemoteSJM(Calculator):
    """Local driver for remote driver, to make parallel stuff easier.
    Initialize with total number of cores supplied (e.g. cores x nodes)
    and regular SJM keywords.
    """

    implemented_properties = SJM.implemented_properties

    def __init__(self, totalcores, atoms=None, *args, **kwargs):
        # FIXME: Would be better to find totalcores from environment.
        Calculator.__init__(self, atoms=atoms)

        # Communications server (via 0MQ).
        context = zmq.Context()
        self.server = context.socket(zmq.REP)
        server_port = self.server.bind_to_random_port('tcp://*')
        self.publisher = context.socket(zmq.PUB)
        publisher_port = self.publisher.bind_to_random_port('tcp://*')

        # Unfortunately gpaw-python doesn't recognize the -m flag,
        # so we need to write the commands to a tempfile.
        # The tempfile will not recognize the atomic_radii function,
        # so we need to hard-code it as well
        # Note: lambda function is fine here, but should not be used in 
        # the script calling RemoteSJM as they are not pickleable. Instead, use
        # def atomic_radii(atoms):
        #     return [vdw_radii[n] for n in atoms.numbers] 
        temp = NamedTemporaryFile(suffix='.py')
        temp.write(b'import sys\n')
        temp.write(b'from pgroup.remotesjm import SJM_Driver\n')
        temp.write(b'from ase.data.vdw import vdw_radii\n')
        temp.write(b'vdw_radii = vdw_radii.copy()\n')
        temp.write(b'atomic_radii = lambda atoms : [vdw_radii[n] for n in atoms.numbers]\n')
        temp.write(b'calc = SJM_Driver(request_address=sys.argv[-2],\n')
        temp.write(b'                   subscribe_address=sys.argv[-1])\n')
        temp.write(b'calc()')
        temp.file.flush()

        # Open real SJM process.
        hostname = gethostname()
        command = 'export OPENBLAS_NUM_THREADS=1 && '
        command += ('srun --mpi=pmi2 -n {:d} gpaw-python {:s}'
                    .format(totalcores, temp.name))
        command += ' tcp://{:s}:{:d}'.format(hostname, server_port)
        command += ' tcp://{:s}:{:d}'.format(hostname, publisher_port)
        self.process = subprocess.Popen(command, shell=True)

        # Wait for all sessions to be set up.
        def broadcast_test_message():
            """From the background."""
            thread = threading.current_thread()
            while True:
                if thread.abort is True:
                    break
                log('broadcast!')
                self.publisher.send_string('test message')
                time.sleep(0.1)
        broadcaster = threading.Thread(target=broadcast_test_message)
        broadcaster.abort = False
        broadcaster.start()

        completed = [1] * totalcores
        log(completed)
        while sum(completed) > 0:
            message = self.server.recv_string()
            log(message)
            rank, message = message.split(':')
            log(message)
            completed[int(rank)] = 0
            log(completed)
            self.server.send(b'')
        broadcaster.abort = True
        log('all subscribers up')
        
        # Now send calculator parameters.
        self.publisher.send_string('time to initialize')
        log(args)
        log(kwargs)
        self.publisher.send_pyobj((args, kwargs))
        log('I think the calculator is set up.')

    def reset(self):
        """Need to also reset the remote calculator."""
        Calculator.reset(self)
        self.publisher.send_string('reset')

    def calculate(self, atoms=None, properties=['energy'],
                  system_changes=all_changes):
        log('RemoteSJM.calculate called!')
        Calculator.calculate(self, atoms=atoms, properties=properties,
                             system_changes=system_changes)
        self.publisher.send_string('calculate')
        # Don't pickle this RemoteSJM instance; this is why we copy.
        send_atoms = None if atoms is None else atoms.copy()

        self.publisher.send_pyobj((send_atoms, properties, system_changes))

        results = self.server.recv_pyobj()
        self.server.send(b'')
        self.results = results

    def __del__(self):
        """Close down SJM."""
        self.publisher.send_string('terminate')
        time.sleep(10.)  # Give time for them to finish.


def log(text):
    with open('log.controller', 'a') as f:
        f.write(str(text) + '\n')


class SJM_Driver:
    """
    This is the "worker" that actually runs SJM, getting parameters and
    atoms from the main script and returning results to it.
    """

    def __init__(self, request_address, subscribe_address):
        log = self.log
        log('SJM_Driver initiating with:')
        log(' request_address ' + request_address)
        log(' subscribe_address ' + subscribe_address)

        # Set up communications channels.
        context = zmq.Context()
        requester = context.socket(zmq.REQ)
        requester.connect(sys.argv[-2])
        self.requester = requester

        subscriber = context.socket(zmq.SUB)
        subscriber.connect(sys.argv[-1])
        subscriber.setsockopt(zmq.SUBSCRIBE, b'')  # XXX Needed?
        self.subscriber = subscriber

        test_message = subscriber.recv_string()
        log('received test message', rank=True)
        log(test_message)
        if test_message == 'test message':
            log('in if test_message', rank=True)
            requester.send_string('{:d}:subscriber working'.format(world.rank))
            requester.recv()

        # Wait for calculator parameters.
        waiting = True
        while waiting:
            message = subscriber.recv_string()
            if message == 'time to initialize':
                waiting = False
        args, kwargs = subscriber.recv_pyobj()

        # Start up real calculator.
        self.calc = SJM(*args, **kwargs)

    def log(self, message, rank=False):
        """if rank is True, all ranks will log; else just rank0."""
        file_open = paropen
        if rank is True:
            file_open = open
            message = str(world.rank) + ':' + message
        with file_open('log.driver', 'a') as f:
            f.write(str(message) + '\n')

    def handle_request(self, message):
        log = self.log
        log('=====================================')
        log('Received <{:s}> request.'.format(message))
        if message == 'calculate':
            atoms, properties, system_changes = self.subscriber.recv_pyobj()
            log('atoms: ' + str(atoms))
            log('atoms.positions: ' + str(atoms.positions))
            log('properties: ' + str(properties))
            log('system changes: ' + str(system_changes))

            self.calc.calculate(atoms=atoms, properties=properties,
                                system_changes=system_changes)
            if world.rank == 0:
                log('sending results: ' + str(self.calc.results), rank=True)
                self.requester.send_pyobj(self.calc.results)
                self.requester.recv()
        elif message == 'reset':
            self.calc.reset()
        elif message == 'terminate':
            self.stay_alive = False  # Exits the infinite loop.
        else:
            raise NotImplementedError('Unknown request: {:d}'
                                      .format(str(message)))

    def __call__(self):
        """Receive and handle requests from remote calculator."""
        self.stay_alive = True
        while self.stay_alive:
            message = self.subscriber.recv_string()
            self.handle_request(message)


if __name__ == '__main__':
    # If gpaw-python were to accept the -m flag, we could do
    # gpaw-python -m pgroup.remote
    # (but now this is not doing anything; the tempfile is used)
    calc = SJM_Driver(request_address=sys.argv[-2],
                       subscribe_address=sys.argv[-1])
    calc()
