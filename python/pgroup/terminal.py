
class Colors:
    """Small class to allow for colored text. Use as in:
        color = Colors()
        # color.disable()
        print(color.RED + 'text to print' + color.END)
    """
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    #RED = '\033[31;1;43m'
    END = '\033[0m'

    def disable(self):
        self.PURPLE = ''
        self.BLUE = ''
        self.GREEN = ''
        self.YELLOW = ''
        self.RED = ''
        self.END = ''
