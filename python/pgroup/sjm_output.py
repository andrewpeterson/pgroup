import numpy as np
from collections import namedtuple
from matplotlib import pyplot


Result = namedtuple('Result', ['subject', 'data'])


class EquilibrationData:
    """Container to hold the data from one equilibratin."""
    name = 'equilibration data'

    def __init__(self):
        self.target_potential = None
        self.tolerance = None
        self.electrons = []
        self.potentials = []
        self.grand_free_energies = []
        self.grand_extrapolated_energies = []
        self.working_slopes = []
        self.regressed_slopes = []


def parse_line(line):
    """The return of this will be a two-item dict,
    with subject and data."""
    if 'Constant-potential mode.' in line:
        return Result('status', 'potential equilibration')
    if ' to equilibrate potential to ' in line:
        potential = line.split('potential to ')[1].split('+/-')[0]
        tol = line.split('+/-')[1].split('V')[0]
        attempt = line.split('Attempt')[1].split('to equilibrate')[0]
        return Result('target potential',
                      {'potential': float(potential),
                       'tol': float(tol),
                       'attempt': int(attempt)})
    if 'Current guess of excess electrons:' in line:
        guess = line.split('electrons: ')[1]
        return Result('electron guess', float(guess))
    if '(Grand) extrapolated:' in line:
        energy = line.split('extrapolated:')[1]
        return Result('grand extrapolated', float(energy))
    if '(Grand) free energy:' in line:
        energy = line.split('energy:')[1]
        return Result('grand free energy', float(energy))
    if 'Potential found to be' in line:
        potential = line.split('to be')[1].split('V')[0]
        return Result('potential', float(potential))
    if 'Potential is within tolerance. Equilibrated' in line:
        return Result('status', 'equilibrated')
    if 'Number of electrons changed to' in line:
        slope = line.split('based on slope of ')[1].split(' V/electron')[0]
        return Result('working slope', float(slope))
    if 'Slope regressed from' in line:
        slope = line.split('attempts is ')[1].split(' V/electron')[0]
        return Result('regressed slope', float(slope))
    if 'After mixing with' in line:
        slope = line.split('new slope is ')[1].split(' V/electron')[0]
        return Result('working slope', float(slope))
    return Result(None, None)


def parse_lines(lines, verbose=True):
    """Parses lines from a GPAW SJM output run, and returns
    a data object containing the potential equilibration results."""
    outputs = []
    for line in lines:
        result = parse_line(line)
        if verbose and result.subject is not None:
            print(result)
        if result.subject == 'status':
            if result.data == 'potential equilibration':
                data = EquilibrationData()
            elif result.data == 'equilibrated':
                outputs += [data]
                data = None
        elif result.subject == 'target potential':
            data.target_potential = result.data['potential']
            data.tolerance = result.data['tol']
            attempt = result.data['attempt']
            if attempt > len(data.regressed_slopes):
                data.regressed_slopes += [np.nan]
        elif result.subject == 'electron guess':
            data.electrons += [result.data]
        elif result.subject == 'potential':
            data.potentials += [result.data]
        elif result.subject == 'grand free energy':
            data.grand_free_energies += [result.data]
        elif result.subject == 'grand extrapolated':
            data.grand_extrapolated_energies += [result.data]
        elif result.subject == 'working slope':
            if len(data.working_slopes) < attempt + 1:
                # This can come in multiple times.
                data.working_slopes += [result.data]
        elif result.subject == 'regressed slope':
            data.regressed_slopes += [result.data]
    if data is not None:
        outputs += [data]
    return outputs


def plot_convergence(equilibration_data, number, axis=None, slopes=False):
    """Plots the convergence for an equilibration data; that is,
    work function versus excess electrons. Optionally an pyplot axis
    can be supplied on which to plot. If slopes=True, adds working
    slope estimate to each point."""
    es = equilibration_data.electrons
    ps = equilibration_data.potentials
    if len(es) > len(ps):
        # Happens when a run is in progress; potential not yet found.
        es = es[:len(ps)]
    tp = equilibration_data.target_potential
    tol = equilibration_data.tolerance
    if axis is None:
        fig, ax = pyplot.subplots()
    else:
        ax = axis
    ax.plot(es, ps, '.k', zorder=5)
    for count, (e, p) in enumerate(zip(es, ps)):
        ax.text(e, p, count, zorder=5)
    ax.set_xlabel('excess electrons')
    ax.set_ylabel('work function, eV')
    xlim = ax.get_xlim()
    ax.plot(xlim, [tp] * 2, '-k', lw=0.2, zorder=2)
    ax.fill_between(xlim, [tp - tol] * 2, [tp + tol] * 2, zorder=1, fc='g')
    # Let's try to add local slopes.
    if slopes:
        ws = equilibration_data.working_slopes
        xrange = xlim[1] - xlim[0]
        for e, p, w in zip(es, ps, ws):
            ax.plot([e - xrange/40., e + xrange/40.],
                    [p - w * xrange / 40., p + w * xrange / 40.],
                    'r-', zorder=2)
    ax.set_xlim(*xlim)
    ax.set_title('Equilibration #{:d}'.format(number))
    if axis is None:
        return fig


def plot_energy(equilibration_data, number=None):
    """Plots the grand extrapolated energy for an equilibration run."""
    energies = equilibration_data.grand_extrapolated_energies
    ps = equilibration_data.potentials
    fig, ax = pyplot.subplots()
    ax.plot(ps, energies, '.k')
    for count, (p, e) in enumerate(zip(ps, energies)):
        ax.text(p, e, count)
    ax.set_xlabel('work function, eV')
    ax.set_ylabel('extrapolated grand free energy, eV')
    ax.set_title('Equilibration #{:d}'.format(number))
    return fig


def plot_slopes(equilibration_data, number):
    """Plots slope estimates used in potential equilibration."""
    es = equilibration_data.electrons
    ws = equilibration_data.working_slopes
    rs = equilibration_data.regressed_slopes
    if len(es) > len(ws):
        # Happens when a run is in progress; potential not yet found.
        es = es[:len(ws)]
    fig, axes = pyplot.subplots(figsize=(8, 10.), nrows=2)
    fig.subplots_adjust(top=0.99, right=0.99, bottom=0.08)
    plot_convergence(equilibration_data, number=number, axis=axes[1],
                     slopes=True)
    ax = axes[0]
    ax.plot(range(len(ws)), ws, 'or', label='working slope')
    ax.plot(range(len(ws)), rs, 'xk', label='regressed slope')
    ax.legend(loc='best')
    ax.set_xlabel('iteration')
    ax.set_ylabel('slope, V/electron')
    return fig
