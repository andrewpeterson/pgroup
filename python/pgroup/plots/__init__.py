#!/usr/bin/env python

import numpy as np
from matplotlib.path import Path
import matplotlib.patches as patches


class PlotStates:
    """Functions to update existing plots with energy levels and connectors.
    Initialize as

    PlotStates(ax, E, halfwidth, textwidth, fontsize, color, textposition,
               text_vspace)

    ax is the axis object to plot to
    E is the (free) energy dictionary, G[state] = value
    halfwidth is half of the width of the horizontal bars
    textwidth is the amount of space to leave for the text labels
    fontsize is the fontsize for the text labels
    color is a matplotlib compatible color specifier
    textposition can be 'inline' for the text to be part of the bar,
        or 'above'/'below' to be above/below the bar, if 'above' or 'below'
        is specified, then text_vspace may be added to move the text farther
        above / below the line
    text_vspace is a float, in units the same as the y axis, of how much
        to move the text above the line
    connect_style is a pyplot linestyle to use for connecting states; e.g.,
        to turn off dashing use '-'
    """

    def __init__(self, ax, E, halfwidth=0.4, textwidth=.2, fontsize=9,
                 color='k', textposition='inline', text_vspace=0.,
                 linewidth=2, connect_style='--'):
        self.ax = ax
        self._E = E
        self._halfwidth = halfwidth
        self._textwidth = textwidth
        self._fontsize = fontsize
        self._color = color
        self._textposition = textposition
        self._text_vspace = text_vspace
        self._linewidth = linewidth
        self._connect_style = connect_style

    def plotstate(self, state, zone, label=None, zorder=2):
        """Plots a horizontal line at the specified energy state. Pulls
        the energy from the E dictionary. State should be fed in as a
        string. zone is the zone number in which to plot it. This is
        generally the total number of electrons transferred to get to
        the state. zone should be fed in as a number (probably an
        integer). By default, puts a text label of the state (number)
        on the bar, but an alternate label can be fed in via the label
        keyword."""
        if label is None:
            label = state
        color = self._color
        lw = self._linewidth
        self.ax.plot((zone-self._halfwidth, zone-self._textwidth),
                     (self._E[state], self._E[state]),
                     color=color, linewidth=lw, zorder=zorder)
        self.ax.plot((zone+self._textwidth, zone+self._halfwidth),
                     (self._E[state], self._E[state]),
                     color=color, linewidth=lw, zorder=zorder)
        if self._textposition == 'inline':
            self.ax.text(zone, self._E[state], label,
                         fontsize=self._fontsize,
                         horizontalalignment='center',
                         verticalalignment='center',
                         color=color)
        elif self._textposition == 'above':
            self.ax.text(zone, self._E[state] + self._text_vspace, label,
                         fontsize=self._fontsize,
                         horizontalalignment='center',
                         verticalalignment='bottom',
                         color=color)
        elif self._textposition == 'below':
            self.ax.text(zone, self._E[state] + self._text_vspace, label,
                         fontsize=self._fontsize,
                         horizontalalignment='center',
                         verticalalignment='top',
                         color=color)

    def connect(self, state1, zone1, state2, zone2):
        """Draws a dashed connector between stated positions and energies.
        Looks up the energies in the E dictionary. state1 and state2
        should be fed in as strings. zone1 and zone2 should be fed
        in as numbers."""
        self.ax.plot((zone1+self._halfwidth, zone2-self._halfwidth),
                     (self._E[state1], self._E[state2]),
                     self._connect_style, color=self._color,
                     linewidth=self._linewidth / 2.)

    def barrier(self, state1, zone1, state2, zone2, barrier, color=None,
                shift=None, lw=None, ls='solid', zorder=2):
        """Draws a curved barrier curve between state 1 and state2 (in
        zone1 and zone2) with a barrier height of barrier. barrier is in
        absolute energy coordinates, not relative to either state.
        shift specifies at what relative point between the initial and
        final states to have the peak of the barrier, and should be in (0,
        1). If shift is not specified, it tries to assign it roughly in
        line with the Hammond-Leffler postulate (early/late)."""
        if lw is None:
            lw = 0.35 * self._linewidth
        energies = [self._E[state1], barrier, self._E[state2]]
        halfwidth = self._halfwidth
        if shift is None:
            dEf = energies[1] - energies[2]
            dEi = energies[1] - energies[0]
            shift = dEi / (dEi + dEf)
            shift = (shift + 0.5) / 2.  # dampen back towards 0.5
        if color is None:
            color = self._color
        peak = (zone1 + halfwidth) + shift * (zone2 - zone1 - 2. * halfwidth)
        bezier3(self.ax,
                [(zone1 + halfwidth, energies[0]),
                 (peak, energies[1]),
                 (zone2 - halfwidth, energies[2])],
                [10., 0., -10], color=color, lw=lw, ls=ls, zorder=zorder)


def bezier3(ax, points, slopes, annotate=False, yscale='linear',
            color='black', lw=1, ls='solid', zorder=2):
    """Draws a series of bezier3 curves with all points on the
    curve and the curve having the specified slope at the point.
    points and slopes are lists of equal lengths. Annotate controls
    whether the backend stuff is displayed on the screen -- mostly
    for troubleshooting. ax is a matplotlib axis object. If yscale
    is given as 'log', then output is made for a logy plot, and the
    slopes are assumed to be log(rise)/run."""

    def find_intercept(line0, line1):
        """Finds the point of interception between two lines. line0 and
        line1 are tuples of (slope,intercept) for both of the lines. Note
        that nan can be fed as a slope (but not an intercept)."""
        if np.isnan(line0[0]):
            raise NotImplementedError()
        elif np.isnan(line1[0]):
            raise NotImplementedError()
        else:
            x = - (line0[1] - line1[1]) / (line0[0] - line1[0])
            y = line0[0] * x + line0[1]
        return x, y

    def line(point, slope):
        """Returns slope,intercept for a line that intersects point. If
        slope is infinite (nan), then it returns the x-intercept in the
        second spot."""
        if np.isnan(slope):
            raise NotImplementedError()
        else:
            intercept = point[1] - slope * point[0]
        return slope, intercept

    # Make list of the bezier-points for CURVE3
    bpoints = []
    prevpoint, prevslope = None, None
    for point, slope in zip(points, slopes):
        if prevpoint is None:
            bpoints.append(point)
            prevpoint, prevslope = point, slope
        else:
            if yscale == 'log':
                lprevpoint = (prevpoint[0], np.log(prevpoint[1]))
                lpoint = (point[0], np.log(point[1]))
                intcp = find_intercept(line(lprevpoint, prevslope),
                                       line(lpoint, slope))
                bpoints.append((intcp[0], np.exp(intcp[1])))
            elif yscale == 'linear':
                intcp = find_intercept(line(prevpoint, prevslope),
                                       line(point, slope))
                bpoints.append(intcp)
            else:
                raise Exception()
            bpoints.append(point)
            prevpoint, prevslope = point, slope
    # Plot it
    if annotate:
        xs, ys = zip(*bpoints)
        ax.plot(xs, ys, 'x--', lw=1, color='black', ms=10)
    codes = [Path.CURVE3] * (2 * len(points) - 2)
    codes.insert(0, Path.MOVETO)
    path = Path(bpoints, codes)
    patch = patches.PathPatch(path, facecolor='none',
                              edgecolor=color, lw=lw, ls=ls, zorder=zorder)
    ax.add_patch(patch)
