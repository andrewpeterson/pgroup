"""To hold convenience functions for creating scaling relations;
e.g., Marcus-like relations."""
import numpy as np
from scipy.optimize import minimize


class DoubleParabola:

    def __init__(self, deltaEs=None, barriers=None, no_parameters=2,
                 b=None, r=None):
        """
        You have two options:
        (1) Initialize with matched lists of energy change of the reaction
        and reaction barriers, and the number of parameters (1 or 2) for the
        model. Will try to fit the model immediately.
        (2) Initialize with the parameters directly; that is supply
        b or (b and r).
        """
        self.parameters = {}
        if b is not None:
            # Must be initializing directly.
            self.parameters['b'] = b
            if r is not None:
                self.parameters['r'] = r
                self._no_parameters = 2
            else:
                self._no_parameters = 1
        else:
            self._deltaEs = deltaEs
            self._barriers = barriers
            self._no_parameters = no_parameters
            self.fit()

    def fit(self, bguess=None, rguess=None):
        if bguess is None:
            # Fitting can be sensitive to initial guess, so
            # estimate b (barrier at deltaE=0) from linear fit.
            # It's equivalent to the intercept.
            bguess = np.polyfit(self._deltaEs, self._barriers, deg=1)[1]
        if rguess is None:
            rguess = 0.49
        if self._no_parameters not in [2]:
            raise NotImplementedError()
        ans = minimize(self._get_loss, x0=[bguess, rguess])
        self.parameters['b'] = ans.x[0]
        self.parameters['r'] = ans.x[1]

    def _get_loss(self, x):
        """Loss function for use with scipy minimize."""
        if self._no_parameters == 2:
            b, r = x
        elif self._no_parameters == 1:
            b = x[0]
            r = None
        loss = 0.
        for deltaE, barrier in zip(self._deltaEs, self._barriers):
            predicted_barrier = self.get_barrier(deltaE, b, r)
            loss += (predicted_barrier - barrier)**2
        return loss

    def get_edges(self, b=None, r=None):
        """Returns the edges of the curved region."""
        if self._no_parameters == 1:
            b = self.parameters['b'] if b is None else b
            return -4. * b, 4. * b
        b = self.parameters['b'] if b is None else b
        r = self.parameters['r'] if r is None else r
        lower = - b / (1. - r)**2
        upper = b / r**2
        return lower, upper

    def get_barrier(self, deltaE, b=None, r=None):
        """Returns the predicted barrier for a given deltaE value.
        Defaults to using fit b (and r) value, but they can optionally
        be supplied. Note if only b is supplied, a one-parameter model
        is assumed.
        """
        no_parameters = None
        if (b is not None) and (r is None):
            no_parameters = 1
        if (b is not None) and (r is not None):
            no_parameters = 2
        if no_parameters is None:
            no_parameters = self._no_parameters
        if b is None:
            b = self.parameters['b']
        if (r is None) and (no_parameters == 2):
            r = self.parameters['r']
        lower, upper = self.get_edges(b, r)
        if deltaE < lower:
            return 0.
        elif deltaE > upper:
            return deltaE
        if no_parameters == 1:
            return (deltaE + 4. * b)**2 / (16. * b)
        else:
            if r == 0.5:
                return self.get_barrier(deltaE, b)
            ans = (1. - r)
            ans *= np.sqrt(1. - deltaE / b * (2. * r - 1.))
            ans -= r
            ans /= 2. * r - 1.
            ans *= ans
            ans *= b
            return ans

    def __call__(self, deltaE, b=None, r=None):
        """Returns the predicted barrier."""
        return self.get_barrier(deltaE, b, r)
