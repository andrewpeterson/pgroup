#!/usr/bin/env python
"""Contains modules to aid in automatically requeuing stopped jobs."""

import time
from threading import Thread
from datetime import datetime

try:
    from ase.parallel import paropen
except ImportError:  # assume no parallel need if no ASE
    paropen = open


class ReQueue:
    """Wrapper for the compute-intensive part of a script, to allow one to
    automatically requeue a job if it doesn't finish. Untested for parallel
    python (e.g., gpaw), where I suspect it might submit 8 jobs if 8 cores
    are running. Use like this:

    requeue = Requeue(maxtime=24., checktime=0.5)
    status = requeue(expensivecommand, *args, **kwargs)
    if status == 'time_elapsed':
        os.system('sbatch --begin=now+2minutes run.py')
    elif status == 'job_completed':
        # do post-job stuff

    Notes:
        1. expensivecommand is something like 'qn.run', fed without quotes
        or parentheses.

        2. maxtime is the time, above which, requeue will kill the job by
        aborting the process. checktime is the frequency at which this
        module will check to see if the process is still running or has
        exited. Make sure that (maxtime - checktime) < clocktime, where
        clocktime is the amount of clock time you allocate for the job in
        the batching system. For example if the clocktime is 25 hours, set
        maxtime to 24.0 and checktime to 0.5.

        3. This cannot return output from the running process. In
        most cases this isn't necessary as the output of DFT calculations
        is written to files, or can be re-called at zero computational
        cost if run from ASE.

    """

    def __init__(self, maxtime=24.0, checktime=0.5, log=None):
        """
        maxtime : time, in hours, at which job will be internally killed.
        Ideally should be just less than the wall time of the job.

        checktime : interval to check if job is still running. This needed
        due to the implementation with python's threading module.

        log : optional path to which to write output
        """
        self._maxtime = maxtime * 3600.
        self._checktime = checktime * 3600.
        self._log = Logger(log)

    def __call__(self, function, *args, **kwargs):
        """Run the function (with optional arguments and keywork arguments)
        until it is killed when elapsed time is greater than maxtime, or it
        notices that the job has terminated. Returns the string
        'time_elapsed' if the job was killed due to a time out or
        'job_completed' if the job finished on its own."""

        log = self._log
        thread = Thread(target=function, args=args, kwargs=kwargs)
        thread.daemon = True
        start = datetime.now()
        thread.start()
        log('Started process: %s' % function)
        log('Will abort after {:.2f} hours.'.format(self._maxtime / 3600.))
        log('Checking every {:.2f} hours.'.format(self._checktime / 3600.))
        elapsed = datetime.now() - start
        elapsedseconds = elapsed.total_seconds()
        while elapsedseconds < self._maxtime:
            time.sleep(self._checktime)
            elapsed = datetime.now() - start
            elapsedseconds = elapsed.total_seconds()
            log('Checking. Elapsed time: {:.2f} hr'
                .format(elapsedseconds / 60. / 60.))
            if not thread.is_alive():
                log(' Job appears finished. Exiting requeue.')
                return 'job_completed'
        log('Max time exceeded. Elapsed minutes: %.2f' %
            (elapsedseconds / 60.))
        return 'time_elapsed'


Requeue = ReQueue  # backwards name compatibility


class Logger:
    """Simple log file utility."""

    def __init__(self, path=None):
        self._path = path

    def __call__(self, text, ts=True):
        """Write text to path. Attach timestamp if ts is True."""
        if not self._path:
            return

        if ts:
            text = time.ctime() + ': ' + text
        f = paropen(self._path, 'a')
        f.write(text + '\n')
        f.close()
