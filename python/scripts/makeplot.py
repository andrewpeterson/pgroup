#!/usr/bin/env python
"""
Starter script for matplotlib figures.
"""

import numpy as np
from matplotlib import pyplot


def makefig(figsize=(5., 5.), nrows=1, ncols=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nrows, ncols: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    """
    nv, nh = nrows, ncols
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)

    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


fig, axes = makefig(figsize=(8., 4.), ncols=3, hr=(1, 2, 3), nrows=2, vr=(8, 2))
fig.savefig('out.pdf')
