#!/usr/bin/env python
"""For each directory in the current level, counts the numbers of files and
subdirectories."""

import os
from optparse import OptionParser

def get_files_from_subdirs(directory):
    """Given a directory (e.g., /home/user/Documents/) and the output of
    os.walk that contains that directory, returns a list of all the files
    in that directory as well as its subdirectories."""
    walk = os.walk(directory)
    files = []
    for descriptor in walk:
        files.extend(descriptor[2])
    return files

def parse():
    """Parse arguments."""
    parser = OptionParser(usage='usage: %prog [directory]')
    (options, args) = parser.parse_args()
    if len(args) == 0:
        return os.getcwd()
    elif len(args) == 1:
        return args[0]
    else:
        raise RuntimeError('Too many arguments.')

if __name__ == '__main__':
    pwd = parse()
    walk = os.walk(pwd)
    walk = [descriptor for descriptor in walk]
    toplevel = walk[0]
    baselength = len(toplevel[0].split(os.path.sep))
    subdirs = [descriptor for descriptor in walk if
               len(descriptor[0].split(os.path.sep)) == 
               (baselength + 1)]
    
    for descriptor in subdirs:
        files = get_files_from_subdirs(descriptor[0])
        print('%5i %s' % (len(files), descriptor[0]))
