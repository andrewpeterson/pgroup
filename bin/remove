#!/usr/bin/env python3
"""Script that duplicates the behavior of rm, but instead just moves the
files into a .Waste folder. Modeled after the bash script rmw, but written
in python.

You need to set the environment variable WASTEDIRECTORY to be the location
you want your waste to be placed in. E.g., you might do this in your .bashrc
like

# Prevent stupid mistakes.
export WASTEDIRECTORY=$HOME/.Waste
alias rm=~/path/to/this/remove
alias permanentremove=/bin/rm
set -o noclobber
alias cp="cp  -i"
alias mv="mv -i"
"""

import os
import time
import shutil
from optparse import OptionParser


def makewastesubdirectory():
    """Makes and returns the name of a subdirectory within the waste
    directory with the current time stamp."""
    try:
        wastedirectory = os.environ['WASTEDIRECTORY']
    except KeyError:
        raise RuntimeError(alert)
    if not os.path.exists(wastedirectory):
        os.mkdir(wastedirectory)
    if not os.path.isdir(wastedirectory):
        raise RuntimeError('Waste directory is not a directory.')
    t = time.localtime()
    string = '%d_%02d%02d_%02d%02d_%06.3f' % (t.tm_year,
                                              t.tm_mon,
                                              t.tm_mday,
                                              t.tm_hour,
                                              t.tm_min,
                                              t.tm_sec + (time.time() % 1))
    subdir = os.path.join(wastedirectory, string)
    os.mkdir(subdir)
    return subdir


alert = """
You need to set the environment variable WASTEDIRECTORY to be the location
you want your waste to be placed in. E.g., you might do this in your .bashrc
like

# Prevent stupid mistakes.
export WASTEDIRECTORY=$HOME/.Waste
alias rm=~/path/to/this/remove
alias permanentremove=/bin/rm
set -o noclobber
alias cp="cp  -i"
alias mv="mv -i"
"""


if __name__ == '__main__':

    parser = OptionParser(usage='usage: %prog path1 path2 ...')
    (options, deletepaths) = parser.parse_args()

    wastesubdir = makewastesubdirectory()

    allnewpaths = []
    for path in deletepaths:

        fullpath = os.path.abspath(path)
        prepath, postpath = os.path.split(fullpath)
        newpath = os.path.join(wastesubdir, prepath[1:])
        if sum([newpath in item for item in allnewpaths]) == 0:
            print('Creating: %s' % newpath)
            print(newpath)
            os.makedirs(newpath)
        allnewpaths.append(newpath)

        print('Moving: %s \n    to: %s' % (fullpath, newpath))
        shutil.move(fullpath, newpath)
